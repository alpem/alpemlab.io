---
title: "JBC Station part 3: working board and a nice case"
date: 2024-09-06T11:30:03+00:00
tags: ["electronics", "projects", "soldering"]
author: "alpem"
showToc: true
TocOpen: false
draft: true
hidemeta: false
comments: false
description: "Desc Text."
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: false
UseHugoToc: true
cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
---

# JBC Station part 3: working board and a nice case

## Introduction

If you are new to this project, here's a quick recap:

- In the first part of this series, I discussed the technical challenges to building a JBC T245-compatible soldering station and different approaches to solving said challenges.
- In the second part, I attempted to build a prototype board based on the ideas discussed in the first part, but later discovered a fatal flaw in my design. 

In the third installment of this series I am presenting an updated (and working!) design based on what I learned with my first prototype, as well as a nice custom-made enclosure. 

## Updated design

As discussed before, the choice of grounding scheme influences the choice of heater switch and thermocouple amplifier. In the last post I experimented with a design containing an earthed control board and an isolated Solid State Relay (SSR). However, it was deemed that this design presented notable design challenges and that this type of switch was far too slow for the application. In consequence, this updated design is based on the more conservative floating control board scheme with a simple high-side switch and an instrumentation amplifier. 

### Power and switch

In this design, incoming AC power is first rectified but only part of it is buffered (to generate the voltage rails). The rectified and non-bufferred source is used to drive the heater in a zero-cross switching scheme. This introduces some inefficiency (in the rectifying diodes) in exchange for flexibility, as this design can be powered from a DC source as well. 

{{< figure src="/img/jbcstation_part3/power.png" width="750" align="center">}}

The MOSFET driving circuitry is shamelessly stolen from the [Unisolder project](https://github.com/sparkybg/UniSolder-5.2). This block is necessary to produce the necessary bias voltage to drive the P-channel MOSFET, which is a challenge given the fact that the Drain-to-Source voltage is not constant but rather a rectified sine wave. Essentially, the driving circuitry buffers Vin to be able to turn off the switch without damaging the part. 

v{{< figure src="/img/jbcstation_part3/switch.png" width="350" align="center">}}

### TC amplifier

Along with the power stages, the thermocouple amplifier block was also changed. AD8221 is also a nicely integrated part, but compared to AD8293G160 it has a user-configurable gain. For this application, a 154Ω resistor was used, giving the amplifier a gain of 321.78. 

{{< figure src="/img/jbcstation_part3/tcamp.png" width="550" align="center">}}

A BAT54 diode clamps the output and prevents damage to the microcontroller.  
