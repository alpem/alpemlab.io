---
title: "Keeping up with the times: DSP-based receiver for ALS162 - Part 1"
date: 2024-11-04T13:20:00+00:00
tags: ["electronics", "radio", "microcontrollers", "projects"]
author: "alpem"
showToc: true
TocOpen: true
draft: false
hidemeta: false
comments: false
description: "Part 1: hardware and design"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: false
UseHugoToc: true
cover:
    image: "/img/als162_rx_part1/portada.png" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
---

## Motivation and an introduction to time signals

A few years ago I built a flipdot-based clock inspired by [a similar project by Marco Reps](https://www.youtube.com/watch?v=TIYhdLQjnYE) but using new displays manufactured by [Alfa-Zeta](https://flipdots.com/en/home/). In the morning I use this clock to get ready at the right time in order to not miss the train. Because the train leaves at a precise time, I have noticed that my clock drifts slightly (I estimate about 10s/month).

{{< figure src="/img/als162_rx_part1/clock.png" title="" width="500" align="center" title="My own electromechanical clock">}}

Having grown annoyed by this phenomenon I started researching ways of keeping it synchronized to a reference time source. However, all of the options had some pitfall or annoyance:
- NTP is ubiquitous, but requires a network connection. Honestly, I don't think a clock should be connected to the internet. 
- GPS offers very high precision, but without the aid of a fast network it's very slow. It also has very poor reception indoors and is relatively power-hungry. 
- Other methods are inconvenient to set up or are inviable for other reasons. I want a solution that works out of the box. 

However, long wave time signals seemed to check all the boxes. The most famous one is DCF77.

### Long wave radio signals and DCF77

Long wave radio signals are radio broadcasts that transmit information about the current time and date. Long wave frequencies are used because they have better range (and the signal has a very low bandwidth anyways). 

Most stations were established in the 1950s and 1960s. Because back then semiconductors were only starting to become a thing, these broadcasts used an AM scheme (easily demodulated using a simple diode envelope detector). The bit rate of the actual time data was established at 1bit/s.

{{< figure src="/img/als162_rx_part1/dcf77module.png" title="" width="450" align="center" title="Cheap DCF77 module sold by Reichelt.">}}

These stations allow authorities to disseminate precise time throughout the country. WWVB (60 kHz) serves this purpose in the U.S., JJY (60 kHz & 40 kHz) in Japan and DCF77 (77.5 kHz) in Germany and neighboring countries. The nearest one to me would be DCF77, so I bought a cheap receiver online to test it. 

### Just one small problem...

I live in Barcelona, and as it turns out the DCF77 signal is almost non-existent here. Under some circumstances, such as at night and in a relatively (electrical) noise-free environment, reception might be possible. However, I failed to recover any useful data with my cheap module. 

{{< figure src="/img/als162_rx_part1/dcf77_range.png" title="" width="300" align="center" title="PTB's very optimistic range estimation for DCF77">}}

With DCF77 off the table, I turned to other alternatives. 


### ALS162

ALS162, formerly TDF162, is the french "alternative" to DCF77. Its original callsign stands for _Télédiffusion de France_, the national french broadcasting company, as this signal was developed to be transmitted on top of _France Inter_'s long wave AM transmission (on 162kHz since 1986). Although its output power has been reduced in recent years, its current spec of 800 kW makes it 16 times more powerful than DCF77's 50 kW transmitter [^1]. This, and the fact that I am physically closer to the station, should mean much better reception.

Because it was designed to be transmitted on top of an already amplitude-modulated signal, ALS162 is modulated in phase, making it an exception amongst time signals. For this reason, **there's no cheap chinese boards to receive it**, as the IC's inside of those are made for amplitude demodulation. Ironically, ALS162 is no longer amplitude modulated, as the _France Inter_ broadcast ended in 2016.

In fact, as far as I can tell, there are very few commercial receivers for ALS162. Most of what I can find is manufactured by [Bodet Time](https://www.bodet-time.com/), which seem to have [some sort of custom ASIC](https://first-tf.com/ressource/asic-composant-de-la-carte-de-reception-permettant-a-une-horloge-de-recevoir-les-signaux-horaires-radio-de-lemetteur-als162-dallouis/) to receive this signal. 

{{< figure src="/img/als162_rx_part1/bodetic.png" title="" width="450" align="center" title="Bodet Time's mysterious ALS162 IC">}}

There's also [one DIY project](https://www.hackster.io/philippedc/als162-an-alternative-to-dcf77-time-radio-with-arduino-uno-bb53fc) which seems to successfully receive and decode ALS162 by using discrete components and analog IC's in combination with a microcontroller, and an [Elektor series (from 2012)](https://archive.org/details/ElektorMagazine/Elektornonlinear.ir2012-09) which, amongst many other things, describes a working ALS162 receiver. Both have some inherent limitations for my intended use, such as requiring a higher voltage power supply, having a high BOM cost or containing some difficult to source parts. 

### In short

The best candidate to keep my clock in sync is ALS162, but I was unable to find a commercial or DIY receiver for it that would suit my needs. I love a good challenge, so I took it into my own hands to develop my own receiver for ALS162. 

## Receiver architecture and some considerations

### Some remarks about ALS162

ALS162, nowadays, is a purely phase-modulated signal. A message is transmitted every minute and is composed of 59 bits of information. Time data (1 bit/s) is transmitted from -50ms to 150ms, with zero being the start of a UTC second. Information is encoded by shifting the phase of the signal linearly upwards up to +1 rad during 25ms, then downwards down to -1 rad during 50ms, then upwards again during 25ms. One of these sequences (let's call them elements) succeeded by 100ms of no phase modulation represents a logical 0, while two of these elements together represent a logical 1.

{{< figure src="/img/als162_rx_part1/phasem.png" title="" width="550" align="center" title="The two symbols used by ALS162 to encode time data">}}

One trivial but important observation is that the angular frequency of a signal is the derivative of its phase. Thus, such a variation in phase can be interpreted as a shift in angular frequency. Thus it can be argued that ALS162 uses a form of Frequency Shift Keying. The frequency deviation is:

$$
\Delta f = \frac{d}{d t} (\Delta \varphi) = \pm \frac{1 \text{ rad}}{25 \text{ ms}} \approx \pm 6.366 \text{ Hz}
$$

One of the main pain points of ALS162 is that this frequency deviation is very small [^2]. For example, relative to the carrier frequency it represents a relative deviation of:

$$
\Delta f^{rel} = \frac{\Delta f}{ f_c } = \frac{6.366}{162000} \approx 39.296 \text{ ppm}
$$

For reference, a typical quartz crystal may offer an accuracy of around 50 ppm. A mismatch between the signal and our reference oscillator may cause phase deviation and clipping. 

{{< figure src="/img/als162_rx_part1/mismphase.png" title="Phase clips due to only 2 ppm transmitter-receiver frequency mismatch" width="600" align="center">}}

The last second in each minute contains no modulation during the whole second (flat region on the figure above). This can be used as a way to sync to the transmission. Out of each second, 200ms are dedicated to time data (i.e., a logical one or zero) and the rest is reserved for other uses and uses a different encoding. 

### Receiver architecture

Firstly, let's consider the most basic functions performed by the receiver:

{{< figure src="/img/als162_rx_part1/rx_steps.png" title="" width="600" align="center">}}

It's clear that:

- **Amplification** must be done in hardware, either by an integrated amplifier or by using discrete components.

- A certain amount of logic is required to **decode** the data, check for parity and output a useful signal. Maybe this can be implemented using discrete logic or some sort of FPGA, but the best option in this case is to use a microcontroller. 

There are various ways to perform **demodulation**, a lot which were developed to be implemented in hardware, as processing power was traditionally very expensive, but others can be implemented in the digital domain. 

### The analog PLL approach

When talking about a phase/frequency-modulated signal the first idea that comes to mind is to use a "jellybean" CD4046 CMOS PLL to lock a VCO to the phase of the signal. 

The main problem, ignoring any performance discussions, is that these VCOs are designed for higher frequency deviations. In fact, the usual mode of operation involves a full frequency range from 0 to 2*fc.

{{< figure src="/img/als162_rx_part1/pllvco.png" title="" width="350" align="center" title="VCO modes of operation">}}

As seen in the figure above, the frequency range can indeed be reduced, but with one gigantic catch:

{{< figure src="/img/als162_rx_part1/pllvcoacc.png" title="" width="350" align="center" title="The center frequency can be off by a huge factor!">}}

While the center frequency could be calibrated, its drift is not well specified and generally it's not how these devices are meant to be used. For example, if no offset is applied, the device is powered by a 3.3V supply and locked to fc, the phase deviation of the signal would make the VCO voltage increase by only:

$$
\Delta V = \frac{3.3 \text{ V} \cdot 6.366 \text{ Hz}}{162000 \text{ Hz}} \approx 130 \text{ μV}
$$

Needless to say, this device is not suitable for this purpose unless the oscillator is replaced. 

#### VCO, VCXO and a catch

Instead, what is needed for such a topology is an oscillator with a small range and very accurate center frequency. This can be achieved by using a Voltage Controlled Crystal Oscillator (VCXO). Such a device is usually constructed with a quartz crystal oscillator with a Varicap as a load capacitor. 

{{< figure src="/img/als162_rx_part1/vcxo.png" title="" width="350" align="center" title="Typical VCXO construction, from Analog Devices">}}


There is a pretty big catch though: because the pull range of a VCXO is small, its frequency has to be divisible to exactly the carrier frequency. The only commercially available crystal frequency that can satisfy this condition is 10.368 MHz/20.736 MHz, [which seems to be used by DECT phones](https://en.wikipedia.org/wiki/Crystal_oscillator_frequencies). Furthermore, a pure crystal is needed (a lot of 10.368 MHz oscillators are actually PLL-based and unsuitable for this application). These devices are not stocked anymore at large electronics distributors and thus I would like to avoid them in this project. 

This is the approach used by the only [DIY endeavour that I have found on the internet](https://www.hackster.io/philippedc/als162-an-alternative-to-dcf77-time-radio-with-arduino-uno-bb53fc). Note how the ATmega microcontroller's own clock is the VCXO, and the divided clock is output via a timer. 

This approach has a two main disadvantages:

- Lack of flexibility: the most critical parameters are set by hardware; for example, the PLL filter dictates its frequency response.

- Requires a 10.368 MHz crystal, which as discussed before I would like to avoid.

### DSP: demodulation in software

To work around the oscillator issue, one option would be to sample the raw RF signal and perform any mixing at full rate using a numerical oscillator. 

The main challenge is that the signal has to be sampled at:

$$
f_s \geq 2 \cdot f_{max}
$$

Where fmax is the highest frequency component of the sampled signal. The RF signal needs some low-pass filtering, as components higher than fs/2 alias into the receiving band. 

{{< figure src="/img/als162_rx_part1/aliasing.png" title="" width="450" align="center">}}

#### LPF considerations

The passband of the anti-aliasing LPF has to be at least 162 kHz (carrier frequency), and the stopband frequency is equivalent to fs/2. Then

{{< figure src="/img/als162_rx_part1/sampling.png" title="" width="800" align="center" title="Two sampling rates and their associated required LPF frequency response (fs1 < fs2)">}}

- An LPF with a higher order has a steeper roll-off slope, but is also more complex. Thus, at a fixed target attenuation an LPF is more complex the smaller its transition region (the gap between the passband and the stopband) is. Therefore, the AA filter grows in complexity as fs decreases.

- A sampling frequency higher than 324 KHz (2*162 kHz) is suboptimal and increases the processing power required (more points/second). 

Thus, when choosing the sampling rate there is an inherent tradeoff between hardware and software cost. 

#### Not so fast

Let's assume that fs = 1 Msample/s. Then, if the processing microcontroller was clocked at, let's say, 100 MHz, then the CPU time/sample would be:

$$
\frac{100 \cdot 10^6}{1 \cdot 10^6} = 100 \ \frac{\text{cycles}}{\text{sample}} 
$$

Even with a very optimized routine and capable microcontroller, 100 RISC cyles are, at best, a couple of arithmetical operations and a few load/stores. As will be seen later, decoding ALS162 is not trivial, so it's not viable to perform all functions at full speed. 


#### Downsampling 

At a theoretical level, our signal occupies quite a narrow band; the high sampling rate is only required because of the carrier frequency. Thus, if the signal could be shifted down the spectrum (through mixing with a sinusoidal oscillator), then the sampling frequency could be reduced through a process known as decimation or downsampling. 

{{< figure src="/img/als162_rx_part1/downsampling.png" title="" width="600" align="center">}}

Because fs is reduced when downsampling (fs' < fs), the maximum representable frequency is lowered. Therefore, the process usually involves two steps:

1. The signal is low-pass filtered to keep only components below fs'/2.
2. The signal is decimated (i.e., only one out of N samples is kept)

The main computational burden of downsampling is the digital filter required to limit the bandwidth before decimation. While there are some very efficient digital filters out there, it's still tricky to implement on a regular microcontroller. However, some STM32 microcontrollers have a trick up their sleve that can help speed up this process. 

#### DFSDM

The DFSDM (Digital Filter For Sigma-Delta Modulators) is a peripheral found in some of ST's STM32 lines. It is marketed as an interface/filter for signals using Pulse Density Modulation (PDM), but it can actually be used as a generic, hardware-accelerated, integrated downsampling block. Thus, it can be used to free up precious CPU time. I got this idea from [this post on mightydevices.com](https://mightydevices.com/index.php/2020/02/stm32l4-discovery-radio-receiver/), which uses this technique to build a Software Defined Radio (SDR).

{{< figure src="/img/als162_rx_part1/dfsdm.png" title="DFSDM block diagram" width="450" align="center">}}

This means that the microcontroller only needs to mix the signal at full rate and offload it to the DFSDM. Then, the signal has a lower sample rate and can be processed more easily. This reduces the necessary computing power (and thus also the literal electrical power required), which is quite desirable. 

## Hardware (rev0)

### The antenna


Like most time signals, ALS162 has a very long wave length:

$$
\lambda = \frac{c}{fc} \approx 1.852 \text{ km} 
$$

Consequently, it's not practical to build antennas that require dimensions as fractions of the wavelength. Instead loop antennas are used, usually with ferrite core to reduce the size (the increased permeability acts as a flux "concentrator", increasing the aparent cross-section of the antenna). These antennas are connected in parallel to a capacitor in order to create a ressonant RLC circuit. 

The main parameters of this circuit are:

- Ressonance frequency: ideally, equal to the carrier, as this is the frequency with maximum gain.

- Q-factor: the relationship between the bandwidth of the antenna to its ressonance frequency. Ideally, as high as possible (ALS162 has a very narrow band). 

In practice, the Q-factor shouldn't be much larger than about 100 to avoid detuning due to changes in the ressonant frequency due to temperature coeficients of the various components (specially the ferrite). 

#### Hacking an aliexpress antenna

For this project, I bougth a DCF77 receiver + antenna for cheap on aliexpress, extracted the ferrite loop and calculated its inductance from the tuning capacitor (using a simple discharge test), assuming it was tuned for DCF77.

{{< figure src="/img/als162_rx_part1/antenna.jpg" title="The manufacturer tunes the antenna by offsetting the coil and the core, lowering its inductance" width="500" align="center">}}

$$
fs \approx 77500 \text{ Hz} \approx \frac{1}{2 \cdot \pi \cdot \sqrt{L C}} 
$$

$$
L = \left( \frac{1}{2 \cdot \pi \cdot 77500 \cdot \sqrt{C}} \right)^2 \approx 3.2 \text{ mH}
$$

Then, I was able to calculate the required new tuning capacitor using the same expression:

$$
C = \left( \frac{1}{2 \cdot \pi \cdot 162000 \cdot \sqrt{L}} \right)^2 \approx 300 \text{ pF} 
$$

This should give us a rough approximation of what capacitance should be added to the ressonant circuit. The rev0 board incorporates a capacitor bank through which the circuit can be tuned to the exact frequency. 

{{< figure src="/img/als162_rx_part1/capbank.png" title="" width="200" align="center" title="Capacitor bank on rev0 board">}}


One issue with this approach is that it can be proven that the Q-factor of an LCR resonator depends on the ratio between L and C (and the parallel resistance R):

$$
Q = R \sqrt{\frac{C}{L}}
$$ 

In this case, C has been modified (lowered) to change the resonance frequency of the oscillator. It can be seen that the Q-factor has been reduced by a factor of:

$$
\frac{Q_\text{DCF77}}{Q_\text{ALS162}} = \sqrt{\frac{C_\text{DCF77}}{C_\text{ALS162}}} = \frac{\frac{1}{2 \pi f_\text{DCF77}}}{\frac{1}{2 \pi f_\text{ALS162}}} = \frac{f_\text{ALS162}}{f_\text{DCF77}} \approx 2.1
$$

This is good enough for now. If a better antenna is needed it can always be replaced afterwards. 
### Amplifier 

The signal from the antenna needs to be amplified in order to be sampled by the ADC. There are two basic requirements:
- High input impedance (in order to maintain a high Q factor)
- ≈80dB amplification (determined experimentally)

{{< figure src="/img/als162_rx_part1/amp.png" title="" width="400" align="center" title="The amplifier input impedance decreases Req and thus the quality factor">}}

The most traditional approach would be to use some sort of FET-based amplifier to achieve high input impedance. However, these types of amplifiers have some unavoidable drawbacks:

- A high supply voltage is needed (i.e., to use the full range of the ADC an additional supply rail is needed).
- Non-linearity of the transistors and non-flat frequency response 
- Idle bias current

In contrast, an opamp-based design circumvents all of these limitations. Nowadays opamps are dirt cheap, and the design requires some anyways for the anti-aliasing LPF, so it was quite a no-brainer. It's quite advantageous to have a certain degree of adjustability of the gain, as it allows the receiver to better use the full range of the ADC. This can be implemented using a digital potentiometer. 

One thing to keep in mind when selecting parts is that real Opamps have a limited slew rate. This makes it so that G * BW = const., where G is the gain of the amplifier and Bw is its bandwidth at that gain. This value is known as the Gain Bandwidth Product and is a characteristic given in the datasheet. This affects this design directly. For example, if GBW=10 MHz (a fairly common value for signal opamps), then:

$$
G_\text{max} = \frac{\text{GBW} }{f_c} = 61.7284 = 35.8 \text{ dB}
$$

This means that with this part a two or three-stage amplifier is required to achieve the desired amplification. In the first revision of the board (rev0) I ended up using three stages, with the third one being a programmable gain amplifier

{{< figure src="/img/als162_rx_part1/amplifier.png" width="650" align="center" title="rev0 contains a lot of jumpers for easier debugging and iteration">}}

Please read the [Errata](#errata-for-rev0) before reproducing this design. 

### Anti-Aliasing filter

For the amplifier I ended opting for an active filter with the following characteristics:

- Sallen-Key topology
- 0.25 dB ripple type I Chebyshev filter response
- -3 dB @ 200 kHz, -40 dB @ 500 kHz 
- 4th order, 2 TLV9062 stages (cheap, 10 MHz GBW, low-ish Iq Opamps).

The values were selected using [Analog Device's Filter Wizard](https://tools.analog.com/en/filterwizard/). The Chebyshev response allows the filter to be built with fewer components compared to a Butterworth-type filter, but with worse phase response. 

{{< figure src="/img/als162_rx_part1/chebyresp.png" width="650" align="center" title="">}}

This isn't really a problem because the phase delay is in the order of single digit microseconds, below the theoretical received accuracy and certainly way below what I hope to achieve here. 

{{< figure src="/img/als162_rx_part1/chebyphase.png" width="650" align="center" title="">}}

The passband is higher than 162 kHz to match the positive ripple of the frequency response to the carrier frequency. In hindsight it's probably not a great idea, as it lowers the attenuation at the stopband, so this will probably change in the next iteration of the board. In any case, the specific frequency response can be modified by changing only the resistors and capacitors, so it's an easy fix. 

### Errata for rev0

- Non-existent decoupling between the analog and digital supplies, resulting in ≈15 mV ripple on the supply lines. 
- LPF response could be optimized.
- The digipot is not suitable for the frequency (the 100k variant is only rated for 100 kHz), and the PGA design produces a very non-linear gain, with very little resolution at high gains. Although this was intentional, it will almost certainly change in a future revision.
- Questionable routing on the antenna area.
- Missing LED resistance.
- Bad display multiplexing

I plan on fixing these mistakes on rev1. I also want to create a display-less variant for integration with other projects. For now I don't advise copying this design, as it has a lot of flaws. 

## More to come

The next installment of this series is going to focus on the software side of the receiver. I will also make the code and hardware available. Stay tuned!

PD: the finale of the JBC station series is coming afterwards, hopefully before 2026. The project itself is mostly done, but writing these entries takes a lot of time. 

[^1]: I know these are probably not comparable, it's just a rough estimate.  
[^2]: Although I can't find any evidence from that time, it's pretty clear that this was a technical compromise to be able to transmit on top of the already existing TDF broadcast without affecting the AM transmission.  

