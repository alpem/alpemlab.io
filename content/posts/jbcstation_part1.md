---
title: "JBC Station part 1: the fundamentals"
date: 2024-02-29T16:00:03+00:00
tags: ["electronics", "projects", "soldering"]
author: "alpem"
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: false
description: "Building a JBC T245-compatible soldering station, part 1"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: false
UseHugoToc: true
cover:
    image: "/img/jbcstation_part1/jbc1-opt1.png" # image path/url
    alt: "An electronics schematic showcasing a proposed driving configuration" # alt text
    caption: "" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
---

## Introduction

JBC is one of the most renowned soldering equipment manufacturers in the world. Most famously, JBC makes the C245 cartridge range, which feature an integrated design containing a thermocouple and heater wire. This construction method offers better performance and can be more compact than older, non thermally bonded tips. 

{{< figure src="/img/jbcstation_part1/jbc1-solderingcomparison.png" title="JBC marketing material: better response times and lower temperature drop" width="500" align="center">}}

The only problem is that these stations are fairly high-end. The cheapest one I can find on their website is 500€ before VAT and shipping. A bit out of my budget, I think :(

On the other hand, the proprietary magic is contained in the cartridge construction. Those are disposable and hence cheap-ish (~30€). 

{{< figure src="/img/jbcstation_part1/jbc1-tip.png" title="C245 tip" width="300" align="center">}}

The tip plugs into a handle (T245) which only contains some contacts and a cable to the station. 

{{< figure src="/img/jbcstation_part1/jbc1-handle.jpg" title="Disassembled T245 handle" width="375" align="center">}}


### State of the art

It doesn't take a genius to realize that the driving circuit/station has a huge markup, so of course the internet is flooded with DIY solutions that can be built for a fraction of the price. Let's take a loop at a non-exhaustive list:

#### Unisolder

[Unisolder](https://github.com/sparkybg/UniSolder-5.2) is a one-size-fits-all solution, meant as a "universal" soldering station. 

{{< figure src="/img/jbcstation_part1/jbc1-unisolder.jpg" title="3d render of a populated Unisolder project" width="450" align="center">}}

It's hugely complex as it supports multiple types of sensors, heaters and includes two channels. As a consequence, the BOM cost is a bit scary.

#### Marco Reps' JBC station

This was the topic of [an old video from Marco Reps](https://www.youtube.com/watch?v=cYgjcDbSyRE). It was constructed with a home-etched PCB. 

{{< figure src="/img/jbcstation_part1/jbc1-reps.png" title="The heatsink is for show, this is a very efficient design" width="500" align="center">}}

It's quite cleverly built, with an isolated solid-state relay made of tiny MOSFETs. However, it requires a 2-output transformer (usually more expensive and bigger) and is made for the T470 handle instead of the T245 handle. 

#### Otter Iron Pro

[Otter Iron Pro](https://github.com/Jana-Marie/Otter-Iron-PRO) is a USB-C PD powered station. It's absurdly tiny, perhaps a bit too much for any practical use

{{< figure src="/img/jbcstation_part1/jbc1-otter.jpeg" title="Otter Iron Pro is barely wider than the connector" width="450" align="center">}}

I mention it here because it uses DC power, unlike the first two projects. However, this driving scheme usually creates a lot of EMI so it should be avoided if possible.

## Technical challenges
 
To decide what's best for our design, let's go over the technical basics.
 
### The tip

The tip (or cartridge, as JBC calls it) exposes three contacts. It is constructed with three coaxial tubes made of different materials and a coil of heating wire. Below is a cross-section of a C245 tip. The center tap (C3) will be called COMM, the outer casing (C1) will be called TC and C2 will be called HEAT.

{{< figure src="/img/jbcstation_part1/jbc1-c245cross.jpg" title="Cross section of a JBC cartridge. The copper tip is missing." width="500" align="center">}}

Because of the [thermoelectric effect](https://en.wikipedia.org/wiki/Thermoelectric_effect), transitions between materials create various thermocouples throughout the circuit. Below is a crude sketch of what is happening inside the tip:

{{< figure src="/img/jbcstation_part1/jbc1-thermocouple2.jpeg" title="" width="650" align="center">}}


The "main" thermocouple is the one created between TC and COMM in the junction of materials M1-M2. One curious effect of this design is that if all the junctions are at the same temperature, M2-Mh-Mh-M1 create a thermocouple equivalent to M1-M2, only in reverse [^1] . This means that the voltage between TC and HEAT is usually (and confusingly) zero. 

For practical purposes, however, this effect can be ignored. The tip is thus represented as follows:

{{< figure src="/img/jbcstation_part1/jbc1-pinout.jpeg" title="" width="500" align="center">}}

As for the technical details:

- The heater has a resistance of about 2.5 Ω at room temperature (the resistivity of the material will increase as the temperature goes up)

- The thermocouple is not a standard type K one. It seems to be a proprietary mix with a linear coefficient of around 23 µV/°C. As far as I know, nobody has characterized it for non-linearity (and it probably doesn't matter much). This means that a gain of \~320 is needed for a 3.3 V ADC.

[^1]: I believe that other JBC irons use this transition as the temperature sensor and ditch the main thermocouple, thus using a series Thermocouple-Heater configuration. 

### Grounding challenges

As you may have noticed, the TC contact is exposed to the exterior. This is intentional: the station is meant to connect this pin to protective earth directly, protecting the operator (and the station) in the event of a fault.

To visualize why this is a problem, let's try a naive solution:

{{< figure src="/img/jbcstation_part1/jbc1-naiveimpl.png" title="A naive solution using a single op amp" width="600" align="center">}}

Because the TC terminal must be connected to PE, the power supply is referenced to PE. As [Marco Reps explains](https://www.youtube.com/watch?v=cYgjcDbSyRE), this creates an escape path for the disruptive heater current to exit through the thermocouple (and perhaps to protective earth). 

### Electromagnetic Interferece (EMI)
 
Although this line of cartridges is rated for around 50 W continuous power, there's a reason that "real" JBC stations are rated for \~130 W. With a heater resistance of 2.5 Ω and 23.5 V as used in official station, these irons can take upwards of 200 W (peak). Connecting such a big instantaneous load on and off _will_ create a lot of EMI. 

{{< figure src="/img/jbcstation_part1/jbc1-cheapst.webp" title="Aliexpress T12 boards are mostly DC switchers" width="450" align="center">}}

For this reason, "respectable" soldering stations use a clever trick: instead of a DC supply, AC power is used. By connecting and disconnecting the heater near or at the zero-crossing point, EMI and transformer humming can be minimized.

{{< figure src="/img/jbcstation_part1/jbc1-zcd.png" title="TI Design Guide 050058 on zero-cross switching" width="625" align="center">}}

## What options are there?

To summarize, the ideal controller board would have:

- A grounded tip
- An AC switching configuration

Additionally, it would be nice to achieve:

- Low BOM cost (and ideally use as few different components as possible)
- Good efficiency
- Compact implementation

After doing some research on existing implementations, I've noticed there are two different ways to approach this problem.

### Option #1: isolated supply and solid state relay 

To avoid the aforementioned grounding problems, this solution uses an isolated heater driving current, driven through an isolated Solid State Relay (SSR). The controller power supply is referenced to protective earth.

{{< figure src="/img/jbcstation_part1/jbc1-opt1.png" title="A simplified schematic for Option #1" width="1000" align="center">}}

This solution was introduced to me by [Marco Reps' video on the topic](https://www.youtube.com/watch?v=cYgjcDbSyRE), but I have since discovered through an [EEVBlog forum post](https://www.eevblog.com/forum/testgear/jbc-soldering-station-cd-2bc-complete-schematic-analysis/) that official JBC stations use the same idea. 

On paper, it has quite a lot of advantages:

- Nearly unbeatable efficiency: with modern, low R<sub>DS(on)</sub> MOSFETs and zero-cross switching, nearly no power is lost to heat. 

- Potential for compact implementation: thanks to its incredible efficiency, no bulky power parts are required. 

- Only requires a single op amp: no instrumentation or differential amplifiers needed.

However, it should also be noted that:

- Off-the-shelf transformers do not come in the necessary voltage and power ratings required for this design. This leaves us with two choices:
	+ Use a larger transformer with more rated power than necessary 
	+ Wire a custom transformer: for mass production, these can be ordered for a reasonable price
	
- Prototyping with AC current is less forgiving than with DC current.

### Option #2: rectified AC switching with differential amplification

This solution uses a single floating power supply and a differential amplifier to read the thermocouple voltage. To simplify the driving circuitry, the incoming AC current is rectified before being used (and part of it buffered to power the controller):

{{< figure src="/img/jbcstation_part1/jbc1-opt2.png" title="A simplified schematic for Option #2" width="1000" align="center">}}

This solution is used by the Unisolder project and by the new PACE ADS200 station, a direct competitor to JBC. 

The disadvantages are quite clear:

- Inferior efficiency due to losses in rectification: this can be mitigated with an active rectifier.

- An instrumentation amplifier [^2] is more complex and expensive than a single low offset voltage op amp.

[^2]: a differential amplifier with input buffer stages is usually called an instrumentation amplifier.

On the other hand, this design offers:

- Greater flexibility on the type of power supply: it's possible to power the station via a DC power supply and use PWM switching. This can be used to validate the design with a protected power source.

- Smaller transformer options: because only one output is needed, the optimal transformer can easily be found on major electronics distributors. 

## More to come

Phew, that's a lot of talking! In the next post I will attempt to assemble a prototype based on these ideas. Stay tuned!
