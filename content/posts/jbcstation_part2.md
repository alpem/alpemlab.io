---
title: "JBC Station part 2: a failed prototype"
date: 2024-03-24T11:30:03+00:00
tags: ["electronics", "projects", "soldering"]
author: "alpem"
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: false
description: "From idea to board, with a sprinkle of mistakes"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: false
UseHugoToc: true
cover:
    image: "/img/jbcstation_part2/protopcb.png" # image path/url
    alt: "Electronics schematic of a Solid State Relay" # alt text
    caption: "" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
---

## A bit of context

If you are new here, I recommend reading [the first part of this series](https://alpem.dev/posts/jbcstation_part1/). There I discussed the various design constraints and considerations and weighed the pros and cons of each one. 

In this post I will discuss the first prototype I designed.

## First prototype

For the first prototype, I wanted to test what in [Part 1](https://alpem.dev/posts/jbcstation_part1/) I called _Option \#1_, more specifically the Solid State Relay (SSR) output stage. The rest of the prototype was simplified a bit to allow me to test the prototype a bit more easily, ending up in a sort of fusion between _Option \#1_ and _Option \#2_. 

{{< figure src="/img/jbcstation_part2/protopcb.png" title="First prototype" width="500" align="center">}}

I won't show the full schematic because I ended up going for a completely different design (post coming soon) but I will try to explain what I learned during the process. 

### Power conversion

Although this block can be solved by throwing components at it, I wanted to keep the BOM cost reasonably low. This configuration features few parts, only a switching converter for the 3.3V rail and a negative linear regulator for the op-amp. 

{{< figure src="/img/jbcstation_part2/power.png" title="Power conversion stage" width="600" align="center">}}

Although this circuit works well, I soon realized that it being limited to AC input (for which I do not have a protected power supply) makes prototyping much more stressful. An unfortunate truth about one-off projects is that development costs are never recovered, so in that sense sometimes the _technically optimal solution_ is not the _best choice_.

### Thermocouple amplifier

Instead of implementing a discrete instrumentation amplifier, I chose to use a single AD8293G160 part. Although the gain is a bit low, it requires no external precision parts (as it has a fixed gain) and is overall a very integrated component.

{{< figure src="/img/jbcstation_part2/tcamp.png" width="500" align="center">}}

However, I can't attest to the performance of this solution because, as will be discussed later, this prototype had a major flaw that prevented further development. 

### Power switch

The power switch was to be one of the main advantages of this design. This isolated Solid State Relay (SSR) only requires two back-to-back n-channel MOSFETs and an admittedly fairly uncommon photocell optocoupler. The MOSFETs can be selected for very low R<sub>DS(on)</sub>, meaning superb performance with a very low BOM cost. 

{{< figure src="/img/jbcstation_part2/ssr2.png" width="600" align="center">}}

Now, at this point I should probably mention that when I designed this (almost three years ago) I had never had any formal electronics training at all; all I knew was random bits of lore passively obtained through watching EEVBlog and Marco Reps videos. Needless to say, I only calculated the most necessary resistor values and eyeballed the rest, so I hope the reader won't judge me too hard for the upcoming amateur mistake.

#### Where it went wrong: MOSFET input capacitance 

Any engineer with half a brain cell was probably frowning at my optimistic description about this circuit. The truth is that I had stolen it from [here](https://www.youtube.com/watch?v=cYgjcDbSyRE) and I didn't think much about it, even though because of the semiconductor shortage in 2021 and 2022 I had to swap the two components of the design. 

One of the limitations of MOSFETs is the parasitic capacitance present between their pins. In the case of the the gate, if the driving source has a high series impedance, the created transient can significantly limit the switching speed and efficiency of the switch (more time in the linear region, more disspated power). 

{{< figure src="/img/jbcstation_part2/paracap.png" width="300" align="center">}}

For this reason, high-frequency applications always use additional driving circuits to quickly turn the MOSFET on or off. The optocoupler specifies a typical open circuit voltage of 8.7V and a short circuit current of 14uA, meaning that its series resistance would be:

$$
R_{s} \approx \frac{8.7}{14 \times 10^{-6}} \approx  621\ k\Omega 
$$

As for the MOSFET, the specified C<sub>ISS</sub> (the sum of C<sub>GD</sub> and C<sub>GS</sub>) is 4556 pF, meaning that the transient created would have a time constant of:  

$$
\tau = RC = 621 \times 10^3 \times 4556 \times 10^{-12} \approx 3 \ m s
$$

Using the standard criterion that stability is achieved after t = 4τ, then a full switch would take:

$$
t = 3 \times 4 = 12 \ m s 
$$

This is way too much for this design, as it doesn't allow for zero-cross switching at 50 Hz and is too slow for a closed loop system. 

## Conclusions

I'm pretty sure this designed would have worked with carefully selected parts or a more complex driving circuit. However, many of the parts I wanted were either out of stock (remember, it's 2021) and/or only available in packages with hidden pads. As you may have guessed from the fact that that I'm building a soldering station, I don't have a fancy hot air tool. 

Finally, I decided to abandon this concept and try _Option \#2_ (as discussed in part 1). 