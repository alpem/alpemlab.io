---
title: "About me"
author: "alpem"
showToc: false
TocOpen: false
draft: false
hidemeta: false
comments: false
description: ""
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: false
ShowBreadCrumbs: false
ShowPostNavLinks: false
ShowWordCount: false
ShowRssButtonInSectionTermList: false
UseHugoToc: true
hideAuthor: true
cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
---

Hi! My name is Pol Rius. I'm currently an undergraduate student studying Industrial Engineering at Universitat Politècnica de Catalunya. I'm expected to graduate in summer 2026. 

I've always had a passion for all things engineering, specially electronics and software, which I have explored over the years through projects developed in my free time. This website is meant to compile these projects, present and past, and discuss their technical details. 

Another interest of mine are trains. In the future, I would like to pursue a carreer in Railway Engineering. 
